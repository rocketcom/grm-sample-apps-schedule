import {PolymerElement, html} from '@polymer/polymer/polymer-element.js';
/* eslint-disable no-unused-vars */
import {RuxTimelineRegion} from './rux-timeline-region.js';
/* eslint-enable no-unused-vars */

import '@polymer/polymer/lib/elements/dom-repeat.js';
import '@polymer/polymer/lib/elements/dom-if';

import template from './rux-timeline-track.html';
import css from './rux-timeline-track.css';

/**
 * @polymer
 * @extends HTMLElement
 */
export class RuxTimelineTrack extends PolymerElement {
  static get properties() {
    return {
      index: {
        type: Number,
        reflectToAttribute: true,
      },
      data: {
        type: Object,
        observer: '_getRegions',
      },
      label: {
        type: String,
      },
      scale: {
        type: Number,
      },
      filter: {
        type: Boolean,
      },
      duration: {
        type: Number,
      },
      playhead: {
        type: String,
        observer: '_updateTrackBackground',
      },
      selectedRegion: {
        type: Object,
        notify: true,
      },
      selected: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
      loaded: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
    };
  }

  isReserved(region) {
    return region.reserved;
  }

  _onClick(e) {
    const _region = e.currentTarget;
    // Still not thrilled with this solution, but this will assign
    // either the model.pass or the subitem object to _model … a
    // I believe this a side effect of having the expanded track
    // be contained within a second loop to the composed track. I.e.,
    // composed tracks have a single loop to add regions, expanded
    // tracks have a loop to create the subtrack, then nested loop
    // to create the reqions.
    const _model = e.model ? e.model.pass : e.subitem;

    if (_region.hasAttribute('selected')) {
      this.selectedRegion = {};
    } else {
      const now = new Date();
      const utcNow = new Date(
          now.getUTCFullYear(),
          now.getUTCMonth(),
          now.getUTCDate(),
          now.getUTCHours(),
          now.getUTCMinutes(),
          now.getUTCSeconds()
      );

      let temporality = 'past';

      if (utcNow.getTime() > _region.startTime && utcNow.getTime() < _region.endTime) {
        temporality = 'present';
      } else if (utcNow.getTime() < _region.startTime) {
        temporality = 'future';
      }

      this.selectedRegion = {
        label: _region.label,
        status: _region.status,
        startTime: _region.startTime,
        endTime: _region.endTime,
        detail: _model.detail,
        groundStation: _model.groundStation,
        iron: _model.iron,
        rev: _model.rev,
        durationMins: _model.durationMins,
        temporality: temporality,
      };
    }
  }

  _getRegions() {
    this.regions = this.data[0].passSlots.filter((region) => {
      return !region.reserved;
    });
  }

  /*
    Update a CSS Custom Property to create the "before/after" effect on the timeline.

    Each track and subtrack has a background set to a linear gradient that simulates
    a two-tone background with a hard edge. The --playhead CSS custom property is set
    whenever the playhead requestAnimationFrame is called in the rux-timeline parent
  */
  _updateTrackBackground() {
    this.style.setProperty('--playhead', this.playhead);
  }

  _onWindowResize() {
    this.trackWidth = this.shadowRoot.querySelectorAll('.rux-timeline__track')[0].offsetWidth;
  }

  constructor() {
    super();

    this._windowListener = this._onWindowResize.bind(this);
  }

  connectedCallback() {
    super.connectedCallback();

    this.trackWidth = this.shadowRoot.querySelectorAll('.rux-timeline__track')[0].offsetWidth;

    window.addEventListener('update', this._windowListener);

    this.loaded = true;
  }

  disconnectedCallback() {
    super.disconnectedCallback();

    window.removeEventListener('update', this._windowListener);
  }

  static get template() {
    return html([
      ` <style include="astro-css">
          ${css}
        </style> 
        ${template}`,
    ]);
  }
}
customElements.define('rux-timeline-track', RuxTimelineTrack);

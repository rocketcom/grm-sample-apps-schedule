# GRM Sample Apps: Schedule

## Description
Ground Resource Management (GRM) operations require ensuring that all the necessary equipment is available during the time windows when a target satellite is in range.  Complicating this task is the fact that there are multiple simultaneous satellite contacts to manage, pieces of equipment that are shared amongst operational groups, and shifting priorities that can require a well-orchestrated schedule to be modified in-flight. Operators need to be able to monitor these impacts to the schedule and make the necessary modifications quickly to ensure that satellite operations can continue.

The GRM Schedule app is designed to allow operators to view and interact with the full schedule of contacts via a Timeline or List View. In addition, it allows the operator to manage the contacts in a single view, including the ability to add, view details, filter, modify and delete contacts.

## Prerequisites
1. A UNIX-like OS (Linux, Mac OS, FreeBSD, etc.)*
2. [Node.js](https://nodejs.org/)
3. [npm](https://www.npmjs.com/get-npm)
4. [Polymer](https://www.polymer-project.org/)
5. [EGS Web Socket Server](https://bitbucket.org/rocketcom/egs-socket-server/src/master/) (optional)**
6. [EGS Web Services Server](https://bitbucket.org/rocketcom/egs-data-services/src/master/) (optional)**

\* It may be possible to run on Windows but has not been thoroughly tested and is not recommended.
\*\* You only need to run your own Web Socket and Web Services servers if you are behind a firewall that prevents you from accessing the ones we provide at <wss://sockets.astrouxds.com> and <https://services.astrouxds.com> respectively.


## Getting Started
### Clone this repository

`git clone git@bitbucket.org:rocketcom/grm-sample-apps-schedule.git`

### Install the Polymer CLI

`npm i -g polymer-cli`

### Install NPM modules for this project

`npm i`

### Create a symbolic link to the appropriate file in the `config` directory.

We recommend linking to config.local.json:

`ln -s config.local.json config.json`

However, if you are running your own Web Socket and Web Services servers (see above) you will need to create your own config file that points to your own servers. Use one of the existing config files as a template and substitute the appropriate URLs.

### For local development and testing

`npm run start`

### To build for production

`npm run build`

### Load the application(s) in your browser

Assuming you've linked your config file to config.local.json:

<http://localhost:8002>

Otherwise, see values from your config file and/or the output from `npm run start`